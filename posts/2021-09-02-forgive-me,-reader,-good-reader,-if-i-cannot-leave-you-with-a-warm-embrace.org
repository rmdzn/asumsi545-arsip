#+title: Forgive me, reader, good reader, if I cannot leave you with a warm embrace 😢
#+date: <2021-09-02 23:32>
#+description: forgive me, reader, good reader, if I cannot leave you with a warm embrace
#+filetags: newsletter januari 2020

Terbit: *20 Januari 2020*
-----
/Weekend is still 5 days away. Suck it up./
-----

[[./image/20jan2020/utama.jpg]]

/Buat kalian yang bercita-cita "mengubah sistem" dari dalam./

** Helmy Yahya menggugat, Dewas TVRI berani?

/Eh, emang benar Helmy Yahya dipecat sebagai Dirut TVRI?/

Helmy Yahya diberhentikan dengan hormat oleh Dewan Pengawas sebagai Direktur Utama TVRI tertanggal 16 Januari 2020.

/Katanya kerjanya bagus, kenapa dipecat?/

Dewan Pengawas TVRI ngasih beberapa alasan:

- Tidak ada penjelasan mengenai pembelian program siaran berbiaya besar seperti Liga Inggris.
- Rebranding TVRI tidak sesuai dengan tata tertib administrasi TVRI.
- Mutasi pejabat struktural tidak sesuai norma, standar, dan prosedur ASN.
- Melanggar asas ketidakberpihakan, kecermatan, dan keterbukaan, terutama soal penunjukan Kuis Siapa Berani.

/Gue nggak peduli sama tiga alasan lain, cuma Liga Inggris. Gue baru nonton TVRI lagi karena ada Liga Inggris./

Ya, itulah. Nggak ada keterangan lebih lanjut dari Dewan Pengawas soal apa yang perlu dijelaskan dari pembelian Liga Inggris.

/Masak belinya nggak pake izin Dewan Pengawas?/

Kayaknya nggak mungkin. Kata Helmy, Dewan Pengawas tahu, kok, karena udah diajak meeting. Bahkan, waktu launching Liga Inggris, Dewan Pengawas hadir.

/Nah lo…/

Helmy malah bilang kalau Dewan Pengawas minta supaya program Liga Inggris bisa meningkatkan penerimaan negara bukan pajak.

/Kayaknya Dewan Pengawasnya kocak, nih. Siapa aja sih anggotanya?/

Nama ketuanya Arif Hidayat Thamrin. Kalau menurut Linkedin, dia orang TV lama. Jadi GM marketing Metro TV 12 tahun. Terus pindah ke Berita Satu. Terakhir, sebelum ke TVRI, dia jadi salah satu direktur iNews.

/Jangan-jangan…/

Hush! Jangan berspekulasi yang aneh-aneh. Mulutmu, UU ITE-mu.

/Ampun! Terus siapa lagi isinya?/

Ada Made Ayu Dwie Mahenny, Supra Wimbarti, Maryuni Kabul Budiono, dan Pamungkas Trishadiatmoko.

/Salah, nggak, kalau gue nggak kenal?/

Cuma Pamungkas yang punya akun Linkedin, jadi bisa ditelusuri rekam jejaknya. Lama di industri TV nasional juga, tapi lama juga kerja di Asian Agri.

/Asian Agri yang perusahaan sawit? Yang diceritain buku ini?/

Tidak lain, tidak bukan.

/Soal yang lain lo nggak bisa ngelacak?/

Made Ayu dari internal TVRI.  Maryuni Kabul lama di RRI. Supra Wimbarti dosen UGM.

/Semuanya sepakat nurunin Helmy Yahya?/

Kecuali Supra Wimbarti. Dilansir Detik, Supra bilang, “Saya enggak ikut tanda tangan. Saya enggak setuju pemecatan itu. Tuduhan-tuduhan itu menurut saya tidak benar.”

/Suara Dewan Pengawas TVRI nggak bulat, dong./

Ada dugaan kalau mentalitas Dewan Pengawas TVRI ini agak lain. Maryuni Kabul, dikutip Media Indonesia, sempat bilang:

“Kita berbeda dengan swasta. Kalau swasta kan orientasi mengundang rating. TVRI itu berbeda. TVRI mencari program yang menarik perhatian publik, sesuai kebutuhannya. Tapi ada pemberdayaan, menggairahkan.”

/Gimana, sih, Pak Kabul? Katanya mau menarik perhatian publik, tapi Liga Inggris malah dipermasalahkan?/

Saya tidak bisa mengabulkan keinginanmu untuk dapat jawaban.

/Helmy Yahya nangis, nggak, ya?/

Dia menyiapkan gugatan hukum. Menurut lawyer Helmy, Chandra Hamzah, pemecatan tersebut tidak sah karena nggak seluruh Dewan Pengawas setuju.

/Kok nama lawyernya familiar ya?/

Dulu pernah jadi komisioner KPK, zaman Cicak vs Buaya.

/Bau-baunya bakal panjang nih urusan. Gas teruuus!/

Di luar gugatan Helmy Yahya, Komisi I DPR RI akan memanggil Dewan Pengawas TVRI untuk meminta keterangan.

-----

/Walaupun hati remuk dan perasaan compang-camping.../

** Laki-laki tetap nggak boleh nangis

/Kata bapak saya, sih, gitu./

Untuk ulang tahun saya yang ketiga, Kakek memberi kado mewah: kandang tiga tingkat sebesar lemari baju. Mungkin itu kondominium di dunia hewan peliharaan.

Segerombolan parkit warna-warni menempati lantai paling atas, sepasang kelinci hitam dan putih di tengah, dan empat ayam kate jantan (waktu itu saya belum tahu apa artinya) di lantai dasar.

Kandang itu jelas bising dan bau. Namun, sepanjang dua bulan pertama, setiap pulang dari taman kanak-kanak, saya menyempatkan diri untuk jongkok dan menyaksikan usaha sia-sia para penghuninya buat membebaskan diri.

Kata Kakek, saya harus menjaga semua hewan, semua jiwa dalam kandang itu, tanpa terkecuali. Saya mengawali kerja mulia itu dengan memberi makan parkit-parkit dengan nasi padang lengkap bersama rendang dan sambal hijau. 

Hampir semua penghuni tingkat teratas mati.

Saya tidak menangis waktu mengubur sepuluh burung parkit di halaman depan. Mereka tak bernama. Hanya Milo dan Moli si kelinci yang punya nama dan sering kupegang dan boleh bermain di luar kandang. 

Milo berwarna hitam dan badannya lebih kecil dari Moli, tetapi lebih gesit dan sibuk. Kata orang-orang Milo jantan, dan suatu hari nanti ia dan Moli akan membuat bayi-bayi kelinci yang lucu.

Suatu hari, tanpa pernah membuat bayi, Milo tewas dipatok ayam. Saya tidak menangis waktu menggali lahatnya.

Selamat jalan, Milo. Terus terang saja, saya lebih nyaman bermain dengan Moli yang gemuk, lembut, dan menggemaskan.

Moli yang rajin menggerogoti wortel dari tangan, selagi saya makan nasi padang lengkap dengan ayam gulai dan tumis daun singkong. Moli yang matanya merah dan bulunya putih dan pantas menjadi anggota terbaru keluarga kami yang nasionalis dan militeristik.

Moli yang mati kesepian ditinggal Milo dan membuat saya menangis sejadi-jadinya.

"Besok kita beli yang baru, atau malah nggak usah. Cuma kelinci. Laki-laki nggak boleh nangis," kata Papa. Saya mengangguk.

Kemudian ayam-ayam kate mati dan sisa parkit dilepaskan. Saya tidak menangis karena laki-laki nggak boleh nangis dan janji harus ditepati.

Walau begitu, di belakang Papa, sebenarnya saya tetap sering menangis, dan setiap butir air mata terasa seperti pengkhianatan.

Menonton Frozen saya menangis, Mendengar Welcome to the Black Parade saya menangis, dan membaca artikel Buzzfeed pun saya menangis. Namun, pengkhianatan itu manis, dan saya berniat menyimpan tangisan sebagai rahasia dengan diri sendiri. Saya masih yakin bahwa laki-laki nggak boleh nangis.

Sampai akhirnya saya melihat Papa menangis suatu malam bertahun-tahun kemudian.

Dia terisak-isak pelan, suaranya yang berat mendengik tinggi. Saya mengintip di balik pintu. Dia tidak memegang tisu seperti Mama kalau menangis, tetapi menyeka air mata dengan tangannya yang langsung kembali mengepal.

Mungkin laki-laki boleh menangis kalau caranya benar.

Paginya, saya bertanya apa yang terjadi. Penjelasan tiba dan kami mengerti dan kami diam di meja makan. Nasi dan dendeng balado juga diam di meja makan. Saya bertanya, pelan dan hati-hati, "Jadi mulai hari ini, laki-laki boleh menangis?"

Papa berdeham, lalu suaranya terdengar lirih sekali, "Nggak, laki-laki nggak boleh nangis...."

• Yudistira Dilianzia
