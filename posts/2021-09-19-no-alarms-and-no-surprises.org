#+title: No alarms and no surprises
#+date: <2021-09-19 22:53>
#+description: no alarms and no surprises
#+filetags: newsletter februari 2020

Terbit: *6 Februari 2020*
-----
/I'll take a quiet life, a handshake of carbon monoxide/
-----
#+attr_html: :alt kover asumsi 545
[[./image/6feb2020/utama.jpg]]

** Mau tahu rahasia Victoria's Secret?
/Jorok sekali dan diawali dengan huruf "M"/

Beberapa tahun lalu, “thigh gap” atau paha bercelah sempat jadi tren kecantikan yang diidamkan banyak perempuan. Agar dianggap ideal, paha perempuan mesti cukup kurus hingga keduanya tidak bersentuhan. Tren ini pertama kali dipopulerkan oleh Victoria’s Secret Fashion Show pada Desember 2012. Saat itu, model-model dengan paha kurus mendapat sorotan, dan media lekas mempopulerkannya sebagai sumber inspirasi.

Sejak itu, muncullah buku, artikel, dan video yang mendorong perempuan untuk punya thigh gap. Buku The Thigh Gap Hack karya Camille Hugh, misalnya, menuliskan kiat-kiat untuk memperolehnya. Ada pula artikel yang terang-terangan menyebut thigh gap pada perempuan akan menarik perhatian laki-laki. Model-model perempuan yang punya thigh gap dianggap sebagai panutan, sementara model yang tidak mencapai standar tersebut dicaci-maki dengan sebutan “babi”, “terlalu gemuk”, dan lainnya.

Padahal, “thigh gap” bukan hanya nyaris mustahil diraih, tetapi juga jadi satu dari sekian tren berbahaya yang membuat perempuan punya obsesi berlebihan terhadap "tubuh ideal," tidak pernah merasa cukup kurus, dan berpotensi membuat perempuan mengalami gangguan makan (eating disorder). 

Walaupun tren tersebut telah surut dan semakin banyak orang yang menyadari pentingnya menerima bentuk tubuh apa adanya, Victoria’s Secret, brand lingerie dan produk kecantikan terbesar di Amerika Serikat, diam-diam tetap saja mempromosikan thigh gap. Situs resminya memamerkan model-model dengan bentuk tubuh yang sama: perempuan bertubuh kurus, punya thigh gap, dan berpose seksi.

Representasi perempuan dalam Victoria’s Secret berbeda 180 derajat dengan brand pakaian dalam Aerie, misalnya, yang menampilkan model-model dengan bentuk tubuh beragam dan lebih sesuai dengan kenyataan—dengan selulit, bekas luka, dan rambut di ketiak.

Ketika New York Times melaporkan tuduhan pelecehan seksual oleh jajaran eksekutif Victoria’s Secret kepada para model dan anggota staf, banyak orang yang tidak kaget. “Investigasi New York Times menduga jajaran senior eksekutif Victoria’s Secret membicarakan perempuan secara merendahkan—tetapi perempuan memang selalu diperlakukan sebagai komoditas [di perusahaan itu],” kata Hadley Freeman dalam The Guardian.  

Laporan yang terbit pada 1 Februari 2020 ini menyorot perilaku Ed Razek, mantan Chief Marketing Officer Victoria’s Secret, yang telah berkali-kali dikomplain karena mencoba mencium model, meminta mereka untuk duduk di pangkuannya, dan menyentuh selangkangan salah satu dari mereka sebelum perhelatan fashion show 2018.

Salah satu mantan model Victoria’s Secret, Andi Muise, mengungkapkan bahwa Razek pernah mengundangnya untuk makan malam berdua, mengiriminya pesan-pesan intim, hingga mengajaknya untuk tinggal serumah. Suatu kali, Razek memintanya untuk datang ke rumahnya untuk makan malam bersama. Muise yang merasa tidak nyaman menolak ajakan tersebut. Setelah penolakan itu, Muise tidak lagi dipilih untuk tampil dalam peragaan busana 2008, meski telah tampil selama empat tahun berturut-turut.  

Ed Razek adalah orang penting Victoria’s Secret. Ia bergabung sejak tahun 1983, punya kuasa tertinggi dalam meng-casting model-model, dan bertanggung jawab terhadap fashion show tahunannya. Razek dikatakan begitu dekat dengan pemilik Victoria’s Secret, Leslie Wexner, hingga karyawan-karyawan Victoria’s Secret menganggap dirinya tak terkalahkan.

Model lain, Alyssa Miller, mendeskripsikan Ed Razek sebagai wajah dari maskulinitas beracun. Miller merangkum perilakunya sebagai: “aku adalah pemegang kuasa. Aku bisa menciptakanmu atau menghancurkanmu.”

Ed Razek pun pernah mengungkapkan keengganannya untuk meng-casting model transgender dan model plus-size. “Menurutku tidak perlu. Kenapa? Pertunjukan ini adalah fantasi, 42 menit hiburan istimewa,” kata Razek dalam wawancara dengan Vogue.com pada 2018. 

Razek bilang Victoria’s Secret telah mencoba membuat program khusus untuk plus-size pada 2000. Namun, menurutnya, “Tak ada orang yang tertarik, sekarang pun masih tidak,” katanya.

Pernyataan Ed Razek mengundang banyak protes, sekaligus mengonfirmasi studi yang menunjukkan bahwa Victoria’s Secret tidak pernah berubah. Brand ini selalu merekrut model-model bertubuh kurus. Bahkan, semakin ke sini, standar tubuh model Victoria’s Secret menjadi semakin kecil—dari lingkar pinggang rata-rata sebesar 62,7 cm pada 1995, menjadi sekitar 59,9 cm pada 2018. Padahal, ukuran tubuh rata-rata perempuan Amerika Serikat terus meningkat. Model Victoria’s Secret pun rata-rata mengenakan pakaian berukuran 3,7—ketika kebanyakan perempuan Amerika Serikat mengenakan ukuran 16.

Model-model Victoria’s Secret juga dilaporkan mesti melakukan program diet yang tak manusiawi demi mencapai ukuran tubuh “ideal” versi Victoria’s Secret. Model Robyn Lawley menyebut program tersebut sebagai “kamp kelaparan”. Model Adriana Lima mengaku tidak lagi mengonsumsi makanan padat sembilan hari sebelum fashion show. Bahkan, ada pula model yang tidak mengonsumsi apa-apa lagi, termasuk cairan. “Dehidrasi adalah masalah besar,” kata salah satu editor fashion kepada The Guardian. 

Pernyataan Razek semakin memojokkan reputasi Victoria’s Secret. Nilai saham Victoria’s Secret sedang menurun drastis, puluhan tokonya di Amerika Serikat dikabarkan akan gulung tikar, dan Razek hanya memperburuk kondisi perusahaan. Protes keras dari publik membuatnya meminta maaf di media sosial. Ia pun memutuskan untuk mengundurkan diri. Tak lama kemudian, Victoria’s Secret juga merekrut model transgender dan plus-size pertamanya, Valentina Sampaio dan Ali Tate.

Namun, terlepas dari pemberitaan-pemberitaan yang memuji Victoria’s Secret dan menganggapnya sedang melakukan terobosan, model plus-size Ali Tate tidak benar-benar direkrut oleh Victoria’s Secret. Perusahaan ini bekerja sama dengan perusahaan lingerie asal UK, Bluebella, yang memang terkenal karena inklusivitasnya. Kontrak Ali Tate berada di bawah Bluebella, bukan Victoria’s Secret.

Dengan munculnya brand-brand pakaian dalam lain yang mengutamakan keragaman tipe tubuh seperti Aerie dan Savage X Fenty, langkah Victoria’s Secret dinilai reaksioner alih-alih revolusioner. “Jika brand ini dikritik karena hanya merepresentasikan satu tipe tubuh perempuan sejak dulu, kenapa baru sekarang menampilkan model plus-size, beberapa minggu setelah berlangsungnya fashion show Savage X Fenty?” kata Amanda Krause dalam artikelnya di Insider. 

Tak hanya Ed Razek, Victoria’s Secret juga dikelilingi dan dipimpin oleh orang-orang bermasalah lain. Fotografer andalan Victoria’s Secret, Russell James, kerap meminta model-model Victoria’s Secret untuk berpose telanjang. Mereka tidak dibayar untuk pekerjaan itu, dan beberapa model menuduh James telah mempublikasikan foto-foto tersebut tanpa persetujuan mereka. Pengelola harta Leslie Wexner, Jeffrey Epstein, diketahui kerap mengaku sebagai agen Victoria’s Secret, mengundang para model baru untuk mengikuti audisi palsu, dan melakukan kekerasan seksual kepada mereka. Leslie Wexner sendiri sering dikatakan gemar merendahkan perempuan.

Dengan proses casting yang diskriminatif, pelatihan atau program diet yang membahayakan kesehatan fisik dan psikis mereka, hingga kondisi kerja yang membuat mereka rentan dilecehkan, Victoria’s Secret dan produk-produknya hadir bukan untuk menjawab kebutuhan perempuan, melainkan memenuhi hasrat seksual laki-laki.

"Rahasia" perusahaan ini, yang sangat jorok, adalah misogini kelas berat.

Audiens mereka pun berada dalam bahaya. Studi “Victoria’s Dirty Secret: How Sociocultural Norms Influence Adolescent Girls and Women” (2008) menyatakan bahwa Victoria’s Secret membuat perempuan selalu merasa kurang. Perempuan menentukan nilai diri mereka dari penampilan fisik, sementara laki-laki mendapat sinyal hijau untuk mengobjektifikasi perempuan. “Apa yang dikatakan Ed Razek kepada transpuan dan perempuan gemuk memang kejam, tetapi perusahaan ini selalu kejam kepada semua perempuan,” kata Hadley Freeman dalam tulisannya di The Guardian.

Sementara itu, Ed Razek menyanggah tuduhan-tuduhan yang disampaikan kepada dirinya. “Tuduhan-tuduhan di laporan ini tidak benar, disalahartikan, atau  diambil di luar konteks,” kata Razek. Menurutnya, ia selalu berkomitmen untuk menghargai orang lain, dan hal ini dibuktikan dari rekam kerjanya dengan model kelas dunia yang jumlahnya tak terhitung. Namun, tidakkah Razek sadar pernyataannya mirip predator-predator seksual kelas internasional seperti Harvey Weinstein di Hollywood dan Roger Ailes di Fox News?

• Permata Adinda
-----
[[https://asumsi.us17.list-manage.com/track/click?u=b00df0d4f32ed7288e4ab5633&id=4a25400b8d&e=589847dd95][file:./image/6feb2020/1.png]]

Kisah pencemaran lingkungan di Lakardowo, Jawa Timur. Pelakunya, aduh lucu sekali, bernama PT PRIA.

Bantu kami sebar kebaikan setiap pagi: bit.ly/545Asumsi
-----
** Laki-laki kok namanya kayak perempuan?
/Ceritanya agak panjang. Santai saja bacanya./

Setiap kali berkenalan dengan seseorang, hal pertama yang saya lakukan adalah menunggu. Lima, empat, tiga... kalau matanya mendelik atau mengerjap-ngerjap dengan cepat, kemungkinan besar obrolan kami akan diawali dengan pertanyaan. Tiga, dua, satu, "Kok namamu Dea?"

Beberapa teman saya merahasiakan nama asli mereka yang feminin dan memilih panggilan yang lebih umum buat laki-laki. Dalam sebuah wawancara, Linda Raharja menanyakan apakah saya pernah terpikir untuk menggunakan pseudonim. Pilihan-pilihan itu tersedia, tetapi saya menyukai nama saya sebagaimana adanya. Ia mudah diingat, bunyinya sedap, dan, kata ayah saya, mengandung harapan agar saya menghormati perempuan sebagaimana saya menghormati diri sendiri.

Selama ini, tanpa malu-malu, saya mengira sudah memenuhi harapan tersebut. Memang saya tidak pernah memperlakukan perempuan secara kurang ajar, juga tak pernah berpikir "sALah sEnDiri paKai RoK pEnDek" atau semacamnya tentang korban kekerasan seksual, misalnya, tetapi apakah itu sudah cukup untuk disebut menghormati? 

Seorang teman pernah memberitahu saya bahwa ada kenalannya yang membenci Bakat Menggonggong dan penulisnya, yaitu saya sendiri, karena cerita-cerita di dalam buku itu "terlalu maskulin." Waktu itu saya membela diri, juga tanpa malu-malu, dengan argumen pinjaman dari novelis Jim Harrison dalam wawancaranya dengan Paris Review: "Masak orang disalahkan hanya karena menulis tentang apa-apa yang dia alami dan ketahui?"

Namun, ketika membaca ulang buku itu untuk keperluan penerjemahannya baru-baru ini, saya merasa penilaian pembaca yang disampaikan teman saya kelewat halus. Mungkin, sebelum mengatakannya, dia sudah menyaringnya bolak-balik. Semua narator dan tokoh utama cerita-cerita saya laki-laki, sedangkan karakter-karakter perempuannya cuma kebagian peran cerewet, sulit diajak bicara, khianat, dan selalu memperkeruh keadaan meski sekadar berjongkok di pinggiran cerita. 

"Sejarah mencatat terlalu banyak lelaki hebat yang tumpas karena gagal menjinakkan perempuan," kata narator cerpen "Sebuah Cerita Sedih, Gempa Waktu, dan Omong Kosong yang Harus Ada." Seorang pembaca berkomentar di Twitter: "Kenapa pula perempuan harus dijinakkan?" Kita memang bisa bicara berbuih-buih bahwa narator atau tokoh utama tidak sama dengan pengarang, bahwa pendapat yang satu belum tentu mencerminkan yang lain, tetapi bagaimana menjelaskan keseragaman cara pandang 14 cerita dalam buku itu selain mengakui bahwa penglihatan saya bermasalah?

Setiap pengarang, kata [[https://asumsi.us17.list-manage.com/track/click?u=b00df0d4f32ed7288e4ab5633&id=7c18b223bc&e=589847dd95][Intan Paramaditha]] dalam esainya "Jalur Feminis pada Pengaruh Sastra", mesti mencari jejak-jejak suara, pikiran, dan cerita para pendahulu perempuan dalam karya masing-masing, dan mencari tahu sebabnya apabila jejak itu tak ketemu.

Saya senang mempelajari puisi-puisi Wislawa Szymborska, Gabriela Mistral, Emily Dickinson, Alejandra Pizarnik, dan banyak penyair perempuan lain. Kalau harus membuat daftar pengaruh atau rekomendasi dadakan, satu atau dua dari nama-nama itu bakal saya cantumkan di tiga besar. Namun, sebagai penulis cerita pendek dan esai, urusan serupa menjadi pelik. Selain Lydia Davis, mungkin semua penulis prosa yang saya anggap hebat dan hendak saya ikuti adalah laki-laki.

Bagaimana dengan penulis perempuan Indonesia? Mungkin, saya bahkan tak membaca Habis Gelap Terbitlah Terang seandainya tak kepepet. Pada 2013, Ubud Writers and Readers Festival menjadikan judul kumpulan surat Kartini itu sebagai tema pokok. Saya tidak pernah tahu mengapa panitia mendudukkan saya, bukan penulis lain, di samping Joost Cote, penerjemah dan pakar Kartini dari Monash University, Australia, dan Ayu Utami dalam diskusi berjudul Kartini's Letters. Panitia mungkin tidak menyangka bahwa saya bakal mempermalukan diri saya dan semua orang hanya beberapa menit setelah pidato pembuka festival oleh Goenawan Mohamad.

Waktu membaca Habis Gelap, saya pikir surat-surat Kartini adalah sumbangan besar pertama Indonesia untuk Global Whining.

"Siapa, sih, orang itu?" tanya Goenawan kepada Zunifah, teman seangkatan saya di kampus yang menjadi relawan festival dan kebetulan berdiri di sampingnya. Zunifah menceritakan itu selepas diskusi. "Keningnya berkerut dan jarinya nunjuk-nunjuk kamu, lho," katanya, lalu tertawa kencang sekali. Saya kabur sebelum bunyi-bunyian itu reda.

Orang-orang mengatakan bahwa selera bacaan tak pernah murni. Ia tak sekadar mengikuti pengetahuan dan pandangan si pembaca, tetapi juga membimbingnya. Ia berjalan di belakang sekaligus di depan. Dan ketika menggembalakan pembaca, selera itu sebenarnya mengikuti jalur yang telah disiapkan pihak lain lewat kanonisasi dan politik kebudayaan. Jika tulisan-tulisan saya tentang perempuan merupakan buah dari kekotoran saya sebagai pembaca, saya kira cara terbaik untuk mengubahnya ialah dengan memperluas bacaan, tanpa peduli arahan selera.

#+begin_quote
Perempuan bukan sekadar konsep. Setiap orang adalah pribadi, bukan setumpuk kopi yang identik dari selembar kertas.
#+end_quote

Mudah saja, pikir saya beberapa bulan lalu, saat mengambil dua novel tipis karya pengarang perempuan, Clarice Lispector dan Sayaka Murata, dari rak toko buku. Sampai sekarang saya belum beres membaca satu pun di antaranya. Satu atau dua halaman, langsung ketiduran. Teman saya Sabda Armandio berkali-kali merekomendasikan, bahkan mau meminjamkan, novel Homegoing karya Yaa Gyasi. Tapi saban kami mengobrol, yang saya tanyakan cuma penyakit tukak lambungnya. Di sisi lain, buku-buku George Saunders dan Yuri Herrera, keduanya pengarang laki-laki, yang saya beli belakangan malah rampung dalam sekali baca.

Siasat berikutnya adalah membaca novel-novel grafis yang dibuat seniman dan penulis perempuan. Berhasil. Saya menikmati Anya's Ghost karya Vera Brosgol, This One Summer karya Mariko dan Jillian Tamaki, dan lain-lain. Memang sebagian besar merupakan cerita coming-of-age, yang cenderung enteng, tapi setidak-tidaknya saya jadi bisa menghibur diri: kesulitan membaca cerita-cerita perempuan pasti bisa terobati.

Dan dua hari lalu, nenek saya dari pihak ibu, yang biasanya selalu kelihatan sibuk bekerja, bercerita tentang ayahnya. Saya pernah mendengar cerita sepotong-sepotong dari Ibu dan Bibi bahwa kakek buyut saya, namanya Tjhia Tjung, datang dari Taipei. Rupanya cerita itu keliru. Kata nenek saya, ayahnya berasal dari provinsi Guangdong di Cina Daratan, dan melarikan diri dari wajib militer--kemungkinan besar untuk Perang Cina-Jepang Kedua (1937-1945) atau Revolusi Komunis Cina (1945-1949). 

Dia tiba di Pulau Bangka dan memutuskan untuk beranak-pinak. Pada akhir 1950-an, Tjung sekeluarga hendak kembali ke Cina tetapi batal karena nenek buyut saya hamil dan takut melahirkan di kapal. Dan kapal tak pernah datang lagi.

Setelah cerita singkat tentang keterampilan kungfu kakek buyut, pelan-pelan minat saya bergeser. Saya jadi lebih banyak bertanya tentang riwayat si juru cerita, nenek saya. Namanya Tjhia Jun Lan dan sang ayah menyayanginya karena dia gesit mengerjakan apa saja, mulai dari menganyam pukat sampai mengangkut kayu-kayu untuk membangun rumah mereka. Sementara itu, ibunya gila judi dan agak sinting. Ketika Jun Lan berumur 16 tahun, ibunya, yang didorong cemburu buta, mengancam: "Kalau bukan dia, aku yang pergi dari rumah", maka Jun Lan mengalah.

Mulai sebagai pembantu rumah tangga yang cuma dibayar makanan, pelan-pelan nenek saya mengumpulkan banyak sekali keahlian: menjahit, mengobras, memasak, membuat kue, memangkas rambut, memelihara segalanya. Dua tahun kemudian ia kawin dengan kakek saya, seorang Melayu, dan memilih untuk menjadi mualaf. Tetapi tentu saja tidak ada penderitaan terakhir. Kakek saya senang main perempuan dan, kata nenek saya, ia menanggung derita itu sendirian selama 17 tahun. 

"Mau cerita ke siapa? Kalau ke adik-adikku, mereka bakal bilang 'siapa suruh kawin sama Melayu?'" katanya. "Kadang aku menangis sambil menguleni adonan kue. Entah sudah berapa banyak orang yang makan air mataku," katanya.

Kapan-kapan, saya akan bercerita lebih rinci. Namun, kembali ke urusan jejak suara, cerita, dan pikiran perempuan, kini saya punya satu riwayat yang bakal saya dekap erat-erat. Lisan saja, tapi berapa banyak di antara kita yang mula-mula berminat kepada cerita berkat kelisanan? Dengan kisah ini, yang bekasnya pada saya tak kalah dalam ketimbang The Old Man and the Sea, misalnya, saya merasa lebih percaya diri untuk mendengarkan lebih banyak kisah dan pikiran perempuan, untuk membaca buku-buku mereka.

Menghormati seseorang, saya kira, mesti mencakup keinginan untuk mendengarkannya, mengetahui pendapat-pendapatnya, mempertimbangkan perasaannya, dan seterusnya, dan seterusnya. Bukan sekadar tak berbuat jahat kepadanya.

"Tapi," kata pasangan saya saat saya memintanya membaca rancangan tulisan ini, "kenapa kamu merasa harus menghormati perempuan sebagaimana kamu menghormati dirimu sendiri?"

Yang jelas, kata saya, bukan untuk lebih memahami perempuan, sehingga bisa menulis lebih baik tentang mereka. Itu keinginan yang tak masuk akal, sebab perempuan bukan sekadar konsep. Setiap orang adalah pribadi, bukan setumpuk kopi yang identik dari selembar kertas.

"Atau, masak cuma untuk memenuhi harapan pada namamu?"

Saya mengatakan sesuatu, yang tak hendak saya tuliskan di sini. Tentu bukan perkara nama. Lagipula, seperti kata Juliet Capulet dalam lakon Shakespeare yang judulnya diketahui semua orang, apa, sih, arti sebuah nama?
 
• Dea Anugrah

*Tulisan ini tayang pertama kali di [[https://asumsi.us17.list-manage.com/track/click?u=b00df0d4f32ed7288e4ab5633&id=7e364b384e&e=589847dd95][Asumsi.co]]
