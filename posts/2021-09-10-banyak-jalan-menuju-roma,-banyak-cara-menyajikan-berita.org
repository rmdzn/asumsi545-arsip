#+title: Banyak jalan menuju Roma, banyak cara menyajikan berita 📧
#+date: <2021-09-10 20:16>
#+description: banyak jalan menuju Roma, banyak cara menyajikan berita
#+filetags: newsletter januari 2020

Terbit: *28 Januari 2020*
-----
/Jangan patah, ya. Jangan hari ini./
-----

[[./image/28jan2020/utama.jpg]]

** Teror berita Corona di grup-grup WhatsApp keluarga

/Penangkalnya sederhana: verifikasi/

*Ada pegawai Badan Pemeriksa Keuangan (BPK)  yang terjangkit flu Wuhan gara-gara bepergian ke Thailand, lalu meninggal dunia.*

Menurut keterangan BPK, benar bahwa karyawan tersebut mengunjungi Thailand pada November 2019, sebelum virus Corona jadi buah bibir, dan kini dia telah meninggal dunia. Namun, karyawan tersebut wafat karena [[https://asumsi.us17.list-manage.com/track/click?u=b00df0d4f32ed7288e4ab5633&id=ee4d72a55a&e=589847dd95][penyakit lain]] yang tidak menular.

*Seorang warga Jambi berkunjung ke Cina, bawa oleh-oleh virus Corona.*

Kemenkes telah membantah hoaks tersebut. Pasien RSUD Raden Mattaher Jambi itu terkena [[https://asumsi.us17.list-manage.com/track/click?u=b00df0d4f32ed7288e4ab5633&id=7ee92e69a9&e=589847dd95][flu biasa]] saja.

Namun, jangan pernah meremehkan bahaya laten influenza. Menurut WHO, virus yang kerap disepelekan ini--bahkan sering dipandang kelewat sepele untuk jadi alasan cuti sakit--dapat menyebabkan 5 juta masalah kesehatan gawat dan membunuh hingga 650 ribu orang setiap tahun.

Dalam beberapa bulan belakangan saja, 26 anak di Amerika Serikat telah meninggal dunia karena influenza.

*Ada yang tewas di Terminal 3 Bandara Soekarno-Hatta. Konon akibat flu Wuhan. Masa hoaks juga? Pakai foto dan tagar #INA_CoronaVirusAlert, lho.*

Korban, RR (76 tahun), adalah penumpang GA-79 rute Bandar Lampung - Jakarta. Memang ia meninggal dunia di bandara, tetapi penyebabnya bukan virus, melainkan [[https://asumsi.us17.list-manage.com/track/click?u=b00df0d4f32ed7288e4ab5633&id=14c070072b&e=589847dd95][serangan jantung]].

Semoga keluarga yang ditinggalkan tabah dalam duka. 

Di atas segalanya, ingatkan diri masing-masing dan para boomer yang kalian sayangi untuk memeriksa kebenaran berita. Kemudian pikirkan cara terbaik untuk menyampaikannya. Salah-salah, kabar buruk dapat menjadi teror, dan ujung-ujungnya justru memicu masalah-masalah kesehatan berat alih-alih bikin waspada.

*[FAKTA] VIRUS CORONA DISEBARKAN PEMERINTAH KOMUNIS CINA UNTUK MEMBASMI UMAT ISLAM!*

Itu bukan fakta, melainkan teori konspirasi. 

Teori konspirasi adalah musuh sains. Jika sains mementingkan ketepatan dan objektivitas, konspirasi sebaliknya. Ia menyediakan jawaban asal-asalan, yang tidak perlu bersandar pada fakta, untuk pikiran-pikiran yang takut dan malas mencari tahu.

Kata Umberto Eco, rezim-rezim fasis kerap memakai konspirasi untuk melempar tanggung jawab sekaligus meraup dukungan politik. Benito Mussolini, misalnya, memastikan agar rakyat Italia percaya bahwa orang-orang Inggris dan Yahudi telah bersekutu untuk menindas mereka. Dengan begitu, dia tak perlu bersusah-susah membenahi negaranya.

Di sejumlah tempat lain, telunjuk terarah kepada orang-orang komunis. "Padahal," kata Eco, "orang komunis udah nggak ada, sekalipun kita mencarinya pakai lampu."

Kembali ke teori ajaib di atas, menurut data 2017, penduduk Wuhan yang beragama Islam sangat sedikit, hanya 1,6%. Ada sangat banyak muslim di dunia ini, buat apa Cina mengacaukan seisi dunia hanya demi memusnahkan 180 ribu orang? Jumlah itu kelewat tak signifikan untuk dimusnahkan dengan virus yang tidak pandang agama dan batas-batas negara. 

Lagipula, pemerintah Cina bekerja sangat serius untuk menangani bencana ini, tanpa pilih-pilih korban. Menurut kabar terbaru, 15 kota telah dikarantina untuk mencegah penyebaran virus, rumah sakit khusus dengan daya tampung 1.000 ranjang dibangun secara kilat, 1.600 personel kesehatan baru dikirimkan ke Wuhan.

*Virus Corona merupakan balasan atas kekejaman pemerintah Cina terhadap masyarakat Uighur.*

Tanpa mengurangi rasa hormat dan dukungan terhadap upaya-upaya menuntut akuntabilitas pemerintah Cina serta keadilan bagi orang-orang Uighur, skenario ini jelas keblinger. Dunia nggak bekerja dengan cara seperti itu, Paklik, Buklik. Mana, coba, hukuman dari langit untuk segala kekejaman di Timor Leste dan Papua? Mana pelipur lara orang-orang sepuh yang setiap Kamis berdiri di depan Istana? 

-----

** 100 hari terasa seperti selamanya

/...kalau kamu korban pelanggaran HAM/

100 hari memerintah, Joko Widodo dan Ma'ruf Amin tak kunjung menunjukkan komitmen untuk menuntaskan kasus-kasus pelanggaran HAM. Demikian menurut Komisi untuk Orang Hilang dan Korban Tindak Kekerasan (KontraS).

Sebagaimana pada masa jabatan 2014-2019, Presiden Joko Widodo masih menempatkan orang-orang yang diduga bertanggung jawab atas berbagai pelanggaran HAM di lingkungan pemerintahannya.

"Kami menyoroti dua nama, yakni Prabowo Subianto, Menteri Pertahanan, dan Wiranto sebagai Ketua Dewan Pertimbangan Presiden," ujar Kepala Biro Penelitian, Pemantauan, dan Dokumentasi KontraS Rivanlee Anandar.

Impunitas atau kekebalan itu tak hanya didapatkan oleh orang-orang lama. 

Kematian Rendi (21) dan Muhammad Yusuf Kardawi (19) di tangan aparat dalam #ReformasiDikorupsi tempo hari tentu masih segar dalam ingatan kita. Tapi apa yang yang dilakukan Polda Sulawesi Tenggara terhadap para pelaku? Sanksi pelanggaran kode etik terhadap enam anggota, dan hanya satu di antara mereka yang diproses pidana. 

"Tapi publik tidak mengetahui enam anggota tersebut diberi sanksi karena menembak demonstran atau karena tidak mengikuti prosedur, seperti tidak ikut apel rutin dan membawa senjata api," kata Rivanlee.

KontraS juga mencatat kenyataan pahit bahwa warga negara yang menggunakan hak konstitusionalnya untuk berunjuk rasa kerap kali dikenakan stigma oleh otoritas, mulai dari "komunis" hingga "makar." Hantaman jahat terhadap demokrasi ini tak pandang bulu. Ia menyasar mahasiswa hingga PNS, para aktivis Papua hingga remaja-remaja yang mengikuti nuraninya untuk turun ke jalan.

Dari segi perundang-undangan, Omnibus Law yang kini dirumuskan juga bikin khawatir banyak orang. Mereka tak membenci investasi, tentu saja. Mereka tahu negeri ini butuh pembangunan dan pembangunan butuh biaya besar. 

Namun, bukan berarti investasi harus jadi kompas, jadi cita-cita bangsa yang haram digugat. Kalau presiden berpikir bahwa segala sesuatu yang berisiko menghambat investasi layak digebuk, ke mana rakyat harus mengadu  saat para pebisnis dan pejabat berlaku sewenang-wenang?

Lagipula, sejak kapan keadilan sosial berarti sedikit orang yang memiliki segalanya boleh merampas dari banyak orang yang hampir tak punya apa-apa?

• Ramadhan Yahya

-----

** Namanya Brusly, dan dia bukan makhluk langka

/Kekerasan seksual bukan hanya penetrasi/

Brusly Wongkar (40 tahun) ditangkap polisi. Sebabnya, dia merancap di depan anak perempuan berusia 10 tahun di sebuah kompleks perumahan di Cikarang Timur, Bekasi. 

Brusly diancam hukuman 10 tahun penjara atau denda sebesar 5 juta rupiah seturut Pasal 36 Undang-undang No. 44 Tahun 2008 tentang Pornografi.

“Menurut pengakuannya sudah nggak bisa dihitung, ratusan kali,” kata Kapolres Metro Bekasi Kombes Hendra Gunawan tentang jumlah kasus Brusly.

Masturbasi atau ekhibisionisme (mempertontonkan alat kelamin) di ruang publik bukanlah kejadian langka di Indonesia. Pada 2019, 964 orang dari 60 ribu responden survei Koalisi Ruang Publik Aman mengaku sebagai korban kejahatan ini.

Menurut studi “[[https://asumsi.us17.list-manage.com/track/click?u=b00df0d4f32ed7288e4ab5633&id=91f7f10024&e=589847dd95][Encountering an Exhibitionist: The Female Victim’s Perspective]]” (2019), lebih dari 60% korban mengalami kejadian itu di tempat umum: taman, kolam, jalan raya, jembatan, dan lain-lain. Hampir semua korban merasa terkejut, jijik, takut, dan marah. Mereka pun dikatakan mengalami trauma jangka pendek dan jangka panjang yang serupa dengan korban-korban kasus kekerasan seksual lain.

Perilaku ekhibisionisme ini tergolong gangguan kejiwaan.

[[https://asumsi.us17.list-manage.com/track/click?u=b00df0d4f32ed7288e4ab5633&id=a6a174e3c6&e=589847dd95][/Psychology Today/]] memperkirakan 2-4% laki-laki mengalami gangguan ekshibisionisme. Perempuan lebih jarang mengalaminya, dan persentasenya belum diketahui.

Menurut psikolog Darrel Turner, ada dua macam kepuasan yang didapatkan oleh para ekshibionis. “Reaksi orang yang ia ekspos, dan aksi mengeskpos itu sendiri ke seseorang yang tidak siap,” kata Turner, dilansir [[https://asumsi.us17.list-manage.com/track/click?u=b00df0d4f32ed7288e4ab5633&id=b5132330dd&e=589847dd95][mic.com]].

Ada elemen kontrol dan kuasa dalam hasrat mengagetkan orang lain tersebut. Turnel mencontohkan kasus kekerasan seksual di penjara, di mana tahanan laki-laki kerap membuka baju dan memperlihatkan alat kelaminnya ke penjaga perempuan. “Ada faktor kendali di sana. Ada perasaan berkuasa, sebab kamu tahu kamu sedang menjadikan orang lain sebagai korban,” katanya.

Menurut psikiater asal San Fransisco, Quandra Chaffers, merancap di depan orang lain tanpa persetujuan adalah kekerasan, bukan aktivitas seksual.

“Ketika seseorang menyerangmu dengan penggorengan, kamu tidak akan bilang dia sedang memasak,” jelasnya lagi.

Selain dinamika kuasa dan keinginan untuk mengontrol, pemahaman yang sempit tentang consent dan kekerasan juga dapat membuat seseorang melakukan kekerasan seksual tanpa mereka sadari. Consent dimaknai sekadar sebagai berkata iya—tak peduli apakah orang tersebut dalam keadaan mabuk, tidak dalam keadaan cukup sadar untuk mengatakan iya, atau mengatakan iya setelah dipaksa atau dimanipulasi.

Kekerasan seksual juga kerap dianggap hanya meliputi penetrasi. Padahal, perbuatan-perbuatan seksual apa pun yang dilakukan tanpa consent adalah bentuk kekerasan seksual—seperti pemaksaan seks oral, menyebarkan gambar intim tanpa seizin subjek gambar, hingga merancap di ruang publik.

“Laki-laki hanya dididik untuk berpikir, ‘aku tidak menahan atau menyerang dia [si korban], kok.’ Pengetahuan kita tentang kekerasan sangatlah sempit,” kata Chaffers.

• Permata Adinda
