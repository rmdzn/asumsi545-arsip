#+title: Come on baby light my fire 🌋
#+date: <2021-09-26 22:48>
#+description: come on baby light my fire
#+filetags: newsletter februari 2020

Terbit: *13 Februari 2020*
-----
/I'm parting the sky between the darkness and me/
-----
#+attr_html: :alt kover asumsi 545
[[./image/13feb2020/utama.jpg]]

** Apakah MD pada Mahfud MD merupakan singkatan dari Mahfud MD?
/Ini lelucon yang panjaaang sekali, seperti kekecewaan kita kepadanya/

Amnesty International dan aktivis Veronica "DPO" Koman mengaku telah menyerahkan data tahanan politik Papua dan korban operasi TNI di Nduga, Papua, kepada Presiden Jokowi pada awal pekan lalu, saat presiden berkunjung ke Canberra, Australia. Kata Veronica, data itu dituliskan secara ringkas dan sederhana agar presiden mudah membacanya.

Ada 57 orang tahanan politik Papua di berbagai penjara, dan operasi militer di Nduga telah menelan nyawa 243 orang, termasuk 110 anak.

Bagaimana tanggapan Menkopolhukam Mahfud MD? Saat ditanyai pada Selasa (11/2), dia berkata, “Belum dibuka kali suratnya. Surat banyak. Kan, surat orang banyak. Rakyat biasa juga ngirim surat ke presiden. Jadi kalau memang ada, sampah ajalah kalau kayak gitu.”

Selain ruwet, jawaban itu menyakitkan hati. Kita tahu, negara ini punya [[https://asumsi.us17.list-manage.com/track/click?u=b00df0d4f32ed7288e4ab5633&id=05b2059259&e=589847dd95][catatan panjang kegagalan menyelesaikan kasus-kasus pelanggaran HAM]], dan sinisme Mahfud, seorang menteri koordinator yang punya wewenang besar, termasuk mengoordinasikan TNI dengan Kementerian Hukum dan HAM, sewajarnya membuat mulut kita terasa dingin dan pahit.

Padahal, dia pernah menyalakan harapan di dada banyak orang. Akhirnya, Menkopolhukam bukan tentara, pikir mereka. Televisi menayangkan Mahfud yang berkemeja putih, tersenyum, melambaikan tangan, melangkah penuh percaya diri ke istana... Akhirnya, pikir kita, seseorang yang kompeten dan punya integritas mendapat kuasa untuk memperbaiki keadaan.

Mungkin kita salah. Para korban, anggota keluarga, pejuang HAM, dan segenap rakyat Indonesia yang mengharapkan keadilan kembali kecewa, kembali berduka, tapi ketahuilah, ini sudah bukan waktunya untuk bersedih tanpa kata-kata.

Katakan bersama-sama: suara rakyat bukan sampah! Daftar nama korban kesewenang-wenangan bukan sampah! Kemarahan kita bukan sampah, melainkan api yang terus membesar. Suatu hari, api itu akan membakar impunitas sampai jadi abu.

*MM Ridho

-----
Kata Mahfud MD, pemerintah kita tidak akan memulangkan eks anggota atau Foreign Terrorist Fighter (FTF) ISIS asal Indonesia. Keputusan itu diambil dalam rapat tertutup bersama Menteri Agama Fachrul Razi, Menkumham Yasonna H. Laoly, Kepala BNPT Suhardi Alius, dan beberapa pejabat lain.

“Wong mereka pergi dari sini secara sukarela, mau diapain?” kata Guru Besar Hukum Internasional UI Prof. Hikmahanto Juwana dalam podcast Asumsi Bersuara with Rayestu episode “Ex Kombatan ISIS mau pulang, kasih ga?” 

Rayestu mengajukan pertanyaan-pertanyaan mendetail. Prof. Hikmahanto menjawab secara artikulatif. Sesekali ia meninggikan suara, menunjukkan passion, namun tetap mendudukkan perkara secara proporsional. Meski berpendirian kokoh, ia sanggup memahami nuansa dalam situasi pelik ini, terutama ketika membicarakan anak-anak.

Kita tahu, tak sedikit anak yang terpaksa ikut keinginan orang tua mereka hijrah dan kini terlunta-lunta. Mereka menderita, jelas, tetapi apakah memanggil anak-anak itu pulang merupakan keputusan terbaik? Bagaimana dengan anak-anak yang terpapar indoktrinasi dan pernah mengikuti latihan militer teroris?

Dengarkan selengkapnya [[https://asumsi.us17.list-manage.com/track/click?u=b00df0d4f32ed7288e4ab5633&id=092f8c0773&e=589847dd95][di sini]]
-----
** Karhutla, oh, Karhutla
/Nggak seperti Karmila dalam lagu Om-om creepy itu, Karhutla ini pasti bukan yang terakhir/

/Bawa pesan ini ke persekutuanmu/
/Hutan dan lahan terbakar lagi/
/Bawa pesan ini lari ke keluargamu/
/Udara kita dikotori lagi/

Status siaga kebakaran hutan dan lahan (karhutla) ditetapkan Pemerintah Provinsi Riau secara resmi pada Selasa (11/2). Data terbaru, Februari 2020, menyatakan kebakaran meluas hingga 271 hektare.

Pemprov Riau mengaku telah memetakan daerah rawan bencana di 346 desa dan 99 kecamatan serta memeriksa ulang izin 387 perkebunan untuk mengetahui status lahan.

Meski mengapresiasi langkah ini dan menyatakan siap membantu, Sekretaris Utama Badan Nasional Penanggulangan Bencana (BNPB), Harmensyah, juga mengingatkan agar Riau mencari solusi permanen untuk mengatasi karhutla kambuhan.

Sekalipun BMKG memprediksi bahwa karhutla akan menurun pada 2020, kabar ini menandai awal yang mengkhawatirkan bagi kelestarian hutan dan lahan Indonesia.

Pada tahun lalu, ada 90.233 hektare hutan dan lahan terbakar. Betul, El Niño mengurangi curah hujan dan membuat kemarau semakin panjang, tetapi di sisi lain praktik land clearing yang berisiko memicu kebakaran tetap dikerjakan oleh berbagai perusahaan.

Alih-alih memperketat regulasi, pemerintah justru menaikkan target produksi bahan bakar hijau (green fuel) dari kelapa sawit. Sebuah unit pengolahan atau kilang baru akan dibangun di Palembang, Sumatra Selatan, untuk memproduksi 20.000 barel bahan bakar per hari. Kilang bernama RUIII Plaju ini menelan biaya Rp32 triliun, dan diharapkan dapat mulai beroperasi pada 2024.

Jadi, apa artinya perkataan Presiden Jokowi bahwa dia bakal mencopot pejabat-pejabat yang dianggapnya bertanggung jawab apabila karhutla terjadi lagi?

"Khusus TNI/Polri yang wilayahnya ada kebakaran besar, hati-hati. Tegas saya sampaikan, pasti saya telepon ke panglima, Kapolri. Kalau ada kebakaran di wilayah kecil kemudian agak membesar, saya tanya Kapolda, Pangdam: 'Kapolres, Dandim-nya sudah dicopot, belum?'" kata Jokowi.

Ada 362 tersangka penyebab karhutla, terdiri dari 345 orang dan 17 korporasi. Pada 2019, kerugian karena karhutla mencapai Rp75 triliun. Tak semua kasus ditangani secara sepatutnya. Bahkan, beberapa perusahaan berstatus tersangka tidak dimintai ganti rugi oleh pengadilan.

*MM Ridho
