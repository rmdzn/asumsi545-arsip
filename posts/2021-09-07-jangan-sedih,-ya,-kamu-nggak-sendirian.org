#+title: Jangan sedih, ya, kamu nggak sendirian :)
#+date: <2021-09-07 23:28>
#+description: jangan sedih ya, kamu nggak sendirian
#+filetags: newsletter januari 2020

Terbit: *24 Januari 2020*
-----
/Aku cemas akan kehilangan kau/

/Aku cemas pada kecemasanku./
-----

[[./image/24jan2020/utama.jpg]]

/Hari ini satu saja: semoga kita tidak lupa./

** Surat pendek tentang kenahasan kita yang panjangnya jutaan tahun cahaya

Saudara-saudaraku yang berbudi baik,

Memperhatikan perilaku boomers di kursi-kursi kekuasaan adalah cara paling sederhana untuk menyadari bahwa kita tidak sedang baik-baik saja. Bak mitraliur habis dilumasi, mereka terus memberondong kita dengan kabar-kabar pencipta sakit kepala. 

Ambil contoh Dewan Pengawas TVRI yang memecat Helmy Yahya karena menyediakan tayangan-tayangan yang tak mencerminkan jati diri bangsa. Atau Majelis Ulama Indonesia (MUI), yang dikabarkan hendak memfatwa haram Netflix--meski mereka lekas menyanggahnya.

Dan tentu saja selalu ada Bapak Johnny G. Plate. Tenang, tenang, saya sudah move on dari permintaannya yang ganjil terhadap Pornhub atau perangnya yang tak kunjung padam melawan Netflix. 

Keberatan saya sekarang: dalam Indonesia Millennial Summit (18/1), beliau menggembar-gemborkan kemajuan infrastruktur digital Indonesia seraya mengimbau agar milenial “ambil bagian di industri” tersebut. “[[https://asumsi.us17.list-manage.com/track/click?u=b00df0d4f32ed7288e4ab5633&id=a8dd792508&e=589847dd95][Milenial perlu kenal kemajuan digital Indonesia]],” katanya.

Boomers yang berkuasa, saya kira, kelewat banyak bicara soal kehidupan anak muda dengan pemahaman yang melenceng entah ke mana. Fenomena ini punya nama: boomersplaining.

Istilah ini mulai berseliweran menjelang Pilpres 2016 di Amerika Serikat, bahkan pernah dituduhkan kepada Bill Clinton saat ia angkat bicara tentang feminisme. Sebelum ucapan "ok boomer" jadi teknik andalan kita untuk membungkam orang-orang uzur salah kaprah, boomersplaining merangkum kecenderungan mereka untuk bacot.

Sebenarnya, wajar bila para boomers penguasa begitu getol ikut campur tanpa diajak. Indonesia diprediksi akan mencapai “bonus demografi” pada 2030, di mana jumlah penduduk berusia produktif menjadi mayoritas. Suara anak muda diperebutkan dalam tiap pemilihan umum, dan istilah “milenial” dilempar oleh setiap instansi pemerintah supaya mereka terlihat cekatan, inovatif, dan berpikiran maju. 

Anak-anak muda adalah aset bangsa, kata mereka.

#+attr_html: :width 500px
[[./image/24jan2020/1.jpeg]]

Saudara-saudaraku yang berpikiran tajam,

Setiap kali orang berbicara tentang anak muda atau milenial, siapakah yang mereka maksud? [[https://asumsi.us17.list-manage.com/track/click?u=b00df0d4f32ed7288e4ab5633&id=d5f9e7675c&e=589847dd95][Penelusuran menarik dari The Jakarta Post]] membeberkan bahwa mayoritas anak muda Indonesia bukanlah pekerja kreatif perkotaan yang gemar menyeruput kopi selagi dirubung nyamuk senja. Kebanyakan “milenial” Indonesia berpendidikan setingkat SMA, bekerja di sektor jasa dengan kedudukan rendah, dan pendapatannya mentok di angka Rp2,1 juta. 

BPS (2017) mencatat hanya 1,4% dari generasi milenial yang menduduki jabatan tingkat manajer dan hanya 7% yang memegang pekerjaan profesional. Sisanya serabutan dan berhamburan bak debu-debu pembangunan. Kepemilikan properti juga telah lama jadi angan-angan hampa: [[https://asumsi.us17.list-manage.com/track/click?u=b00df0d4f32ed7288e4ab5633&id=8eaa6951e7&e=589847dd95][81 juta milenial di Indonesia]] belum memiliki rumah.

Ariane Utomo, seorang ahli demografi sosial yang diwawancarai The Jakarta Post, menyatakan bahwa prediksi bonus demografi “bisa jadi bersandar pada asumsi yang tidak lagi tepat”. Indonesia tengah mengalami “deindustrialisasi prematur” yang menghabisi sektor manufaktur, dan milenial saat ini menghadapi perubahan besar-besaran tentang definisi “kerja” itu sendiri.

[[https://asumsi.us17.list-manage.com/track/click?u=b00df0d4f32ed7288e4ab5633&id=eebb5f2618&e=589847dd95][Menurut Pete Robertson dari Universitas Edinburgh Napier]], karier pada abad 21 akan menjadi “tanpa batas, berdasarkan portofolio, dan protean (pekerja mengubah diri sesuai kebutuhan).” Prediksinya kelam: tak ada lagi pekerjaan seumur hidup. Semua orang harus bergelantungan pada dua, tiga, empat pekerjaan. Mereka berayun dari satu proyek ke proyek lainnya tanpa kepastian jaminan kerja dan jenjang karier. Di masa depan, mau tidak mau, semua orang akan menjadi pekerja prekariat.

[[https://asumsi.us17.list-manage.com/track/click?u=b00df0d4f32ed7288e4ab5633&id=ebcade7eeb&e=589847dd95][Pekerjaan-pekerjaan musiman]] (ingat artikel 5.45 soal gig economy tempo hari?) macam ini bakal menciptakan stagnasi ekonomi. Para pekerjanya terperosok ke dalam “jebakan keterampilan”. Seorang pengemudi ojol, misalnya,  berpeluang besar selamanya jadi pengemudi ojol, manunggaling kawula-Honda. Tak ada peluang mendapat keterampilan baru, apalagi kemajuan karier. Dan berkat kemerosotan ekonomi, semakin lama akan semakin banyak yang terjerumus ke lubang ini.

Retorika “anak muda aset bangsa” juga punya masalah mendasar yang terang-benderang. Kita dipandang sebagai sekadar aset, bukan manusia utuh yang punya berbagai kerumitan. 

Ada anekdot yang menyakitkan soal ini: kemarin, seorang karyawan di Gedung BRI 2, Jakarta, dicurigai mengidap virus Corona yang mewabah dari Cina. Setelah gedung dikarantina, rupanya yang bersangkutan cuma sakit tenggorokan. Menteri Kesehatan Terawan Agus Putranto lantas meratapi “kerugian” ekonomi dari penutupan gedung tersebut. Astaga, berapa harga nyawa kita sekarang?

Semestinyalah kita tidak terkejut. Omnibus Law Penciptaan Lapangan Kerja terus digas meski ditolak para buruh yang merasa rancangan undang-undang tersebut berat sebelah. Karpet merah dibentangkan untuk industri batu bara meski dampaknya terhadap kerusakan lingkungan telah terbukti di sana-sini. Iuran BPJS dinaikkan berkat anggapan “jika sehat itu murah, orang menjadi sangat manja.” Apabila rakyat--termasuk anak muda--dipandang sekadar sebagai aset, kita bakal diperlakukan seperti barang.

Hal inilah yang membuat boomersplaining begitu menjengkelkan. Pejabat-pejabat pemerintahan kita tak hanya sembrono mendefinisikan anak muda Indonesia, tetapi juga mengamalkan kebijakan-kebijakan yang bikin kita terkencing-kencing di celana.

-----
#+begin_quote
Kebanyakan “milenial” Indonesia berpendidikan setingkat SMA, bekerja di sektor jasa dengan kedudukan rendah, dan pendapatannya mentok di angka Rp2,1 juta.
#+end_quote
-----

Betapa aneh rasanya mendengar seseorang mengimbau anak muda agar memahami kemajuan digital sementara ia sendiri seperti datang dari abad pertengahan. Ganjil sekali mendengar anggota dewan pengawas stasiun TV nasional bahwa Liga Inggris tak mencerminkan jati diri bangsa, sementara kita tahu bangsa bisa terus berubah dan toh anak-anak muda juga yang akan menentukan jati dirinya di masa depan.

Mungkin kita harus menunggu waktu mengerjakan tugasnya, membiarkan yang tua-tua membusuk dan tergantikan dengan sendirinya. Tetapi menunggu sekadar menunggu, saya kira, takkan mengubah keadaan.

Bila kita diam, para penguasa di masa depan bakal berperangai sama sebab datang dari lingkungan elite yang itu-itu saja. Katakanlah, ada Cucu Singkong dalam stafsus "milenial" presiden, tetapi mengapa tak ada cucu tukang becak atau anak mbok-mbok buruh cuci?

"Memangnya kita bisa apa?" katamu. "Kita terlampau lemah dan beginilah cara dunia bekerja."

Kita, saudaraku, bisa melawan dengan cara yang sederhana, yang kecil saja tetapi bermakna. Kita bisa berhenti menganggap serius para boomers penguasa dan mulai mengorganisir diri, menyemai gagasan-gagasan baru, serta mengangkat derajat satu sama lain. 

Masa depan kita tidak boleh ditentukan oleh orang-orang yang kelak takkan ada untuk melihatnya!

Salam,

• Raka Ibrahim & Dea Anugrah
